package test;
import static org.junit.Assert.*;

import org.junit.Test;


public class NumerosRomanosTest {

	@Before
	Conversor conversor;
	public void setup() throws Exception{
		conversor = new Conversor();
	}
	@Test
	public void testRomanosParaArabicos() {
		assertEquals(1,conversor.RomanosParaArabicos("I"));
		assertEquals(2,conversor.RomanosParaArabicos("II"));
		assertEquals(10,conversor.RomanosParaArabicos("X"));
	}
	@Test
	public void testArabicosParaRomanos(){
		assertEquals("I",conversor.ArabicosParaRomanos(1));
		assertEquals("II",conversor.ArabicosParaRomanos(2));
		assertEquals("X",conversor.ArabicosParaRomanos(10));
	}
	
}
